// liveformat.cpp : 定义控制台应用程序的入口点。
//
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <windows.h>
#include <conio.h>
#include <io.h>
#include <dos.h>
#include <direct.h>
#include <time.h>
#include <cmath>
#define Max 500
#define  MaxLen 248000
#define m3u 0
#define txt 1
typedef struct
{
	int LiveKindCnt;		   //台数
	char LiveKindName[Max][50];  //台名
	int LiveKindSite[Max][30];   //每个台地址序号
	int LiveKindSiteCnt[Max];   //台地址个数
}LiveDataStruct;
typedef struct
{
	int SetCnt;		   
	char GroupName[100][100]; 

}GroupSet;
typedef struct
{
	int GroupCnt;		   
	char GroupName[50][100];  
	GroupSet Groupset[50];   

}LiveConfig;
int main(int argc, char* argv[])
{
	FILE *m3uFile = NULL,*liveSelf = NULL,*ResultFile = NULL,*configFile = NULL;
	// 	liveSelf  = fopen("live.ini","r");
	// 	if(liveSelf == NULL)
	// 		return 0;	

	ResultFile  = fopen("proxylive.json","w");
	if(ResultFile == NULL)
		return 0;

	configFile  = fopen("config.ini","r");
	if(configFile == NULL)
		return 0;
	char buff[MaxLen] ={0},bufftemp[MaxLen] = {0},buffChange[MaxLen] = {0},sitetemp[Max] = {0};



	//fputs(buff,ResultFile);
	char *p1 = NULL;
	char cLine[1024] = {0};
	int istart = 0,p  = 0 ,q = 0;
	char *p2 = NULL,*p3 = NULL,*p4 = NULL;
	char LiveName[Max] = {0}, livesite[Max*2][Max] = {0};
	char buffSet[MaxLen] = {0};

	//预处理
	memset(buffSet,0,sizeof(buffSet));
	LiveDataStruct LiveData;
	int iget = 0,iGot = 0,i = 0 ,j = 0,k,n,SiteCnt = 0,iset = 0;
	int istep = 0,iFileType  = 0,keySet = 0;
	char strtemp[1024] = {0};
	int iHasDir=0;
	struct _finddata_t data;
	char PathStr [100] = {0};


	LiveConfig Config;
	memset(&Config,0,sizeof(LiveConfig));

	while(!feof(configFile))
	{
		memset(cLine,0,sizeof(cLine));
		fgets(cLine,sizeof(cLine),configFile);
		if(cLine[0] == 0)
			continue;

		p1 = strstr(cLine,"#");
		if(p1)
		{
			memset(Config.GroupName[Config.GroupCnt],0,sizeof(Config.GroupName[Config.GroupCnt]));
			memcpy(Config.GroupName[Config.GroupCnt],cLine,p1-cLine);
			p1 = p1+1;
			while(p1)
			{
				p2 =strstr(p1,"/");
				if(p2)
				{
					memset(Config.Groupset[Config.GroupCnt].GroupName[Config.Groupset[Config.GroupCnt].SetCnt],0,sizeof(Config.Groupset[Config.GroupCnt].GroupName[Config.Groupset[Config.GroupCnt].SetCnt]));
					memcpy(Config.Groupset[Config.GroupCnt].GroupName[Config.Groupset[Config.GroupCnt].SetCnt],p1,p2-p1);
				}
				else
					break;
				p1 = p2+1;
				Config.Groupset[Config.GroupCnt].SetCnt++;
			}
			Config.GroupCnt++;

		}
		else
			continue;
	}
	long hnd;int  nRet,iw = 0,icut = 0;
	for(q = 0,icut = 0;q<Config.GroupCnt;q++)
	{
		if(!memcmp(Config.GroupName[q],"电影",6))
			icut = 1;
		else
			icut = 0;
		for(p = 0,iw = 0;p<Config.Groupset[q].SetCnt;p++)
		{
			SiteCnt = 0;
			LiveData.LiveKindCnt = 0;
			for(i = 0 ;i<Max;i++)
			{
				memset(LiveData.LiveKindName[i],0,sizeof(LiveData.LiveKindName[i]));
				memset(LiveData.LiveKindSite[i],0,sizeof(LiveData.LiveKindSite[i]));
				LiveData.LiveKindSiteCnt[i] = 0;
			}
			memset(livesite,0,sizeof(livesite));
			memset(PathStr,0,sizeof(PathStr));
			strcpy(PathStr,"*.*");//table3文件目录
			printf(PathStr);

			hnd = _findfirst(PathStr, &data );
			if ( hnd < 0 )
			{   
				printf(" can not find .txt file\n");
				return 0;
			}

			nRet = (hnd <0 )?-1:1;
			while ( nRet >= 0 )
			{
				if ( data.attrib == _A_SUBDIR )
				{// 如果是目录            
					if (strcmp(data.name,".")==0 ||strcmp(data.name,"..")==0)
					{
						nRet = _findnext( hnd, &data );
						continue;
					}
					iHasDir=1; 
				}
				else
				{
					// 如果是目录            
					if (strcmp(data.name,".")==0 ||strcmp(data.name,"..")==0)
					{
						nRet = _findnext( hnd, &data );
						continue;
					}
					else   
					{
						p1 = NULL;
						if(strstr(data.name,"txt"))
						{
							p1 = strstr(data.name,".txt");
							iFileType = txt;
						}
						else if	(strstr(data.name,"m3u"))
						{
							p1 = strstr(data.name,".m3u");
							iFileType = m3u;
						}

						if(p1)
						{
							sprintf(PathStr,"%s",data.name);
							m3uFile  = fopen(PathStr,"r");
							if(m3uFile == NULL)
								return 0;
							while(!feof(m3uFile))
							{
								memset(cLine,0,sizeof(cLine));
								fgets(cLine,sizeof(cLine),m3uFile);
								if(iFileType == m3u)
								{
									if(istart == 0)
									{
										p1 = strstr(cLine,Config.Groupset[q].GroupName[p]);
										int ilen = strlen(Config.Groupset[q].GroupName[p]);
										if(p1)
										{
											istart = 1;
											//保存名字
											p2 = strstr(cLine,",");
											if(p2)
											{
												p3=strstr(p2+ilen," ");
												if(p3)
												{
													memset(LiveName,0,sizeof(LiveName));
													memcpy(LiveName, p2+1,p3-(p2+1));
												}
												else
												{
													p3=strstr(p2+ilen,"台");
													if(p3)
													{
														memset(LiveName,0,sizeof(LiveName));
														memcpy(LiveName, p2+1,p3-(p2+1));
													}
													else
													{
														p3=strstr(p2+ilen,"高清");

														if(p3)
														{
															memset(LiveName,0,sizeof(LiveName));
															memcpy(LiveName, p2+1,p3-(p2+1));
														}
														else
														{
															p3=strstr(p2+ilen,"FHD");

															if(p3)
															{
																memset(LiveName,0,sizeof(LiveName));
																memcpy(LiveName, p2+1,p3-(p2+1));
															}
															else
															{
																p3=strstr(p2+ilen,"HD");

																if(p3)
																{
																	memset(LiveName,0,sizeof(LiveName));
																	memcpy(LiveName, p2+1,p3-(p2+1));
																}
																else
																{

																	p3=strstr(p2+ilen,"[");

																	if(p3)
																	{
																		memset(LiveName,0,sizeof(LiveName));
																		memcpy(LiveName, p2+1,p3-(p2+1));
																	}
																	else
																	{
																		p3=strstr(p2+ilen,"(");

																		if(p3)
																		{
																			memset(LiveName,0,sizeof(LiveName));
																			memcpy(LiveName, p2+1,p3-(p2+1));
																		}
																		else
																		{
																			memset(LiveName,0,sizeof(LiveName));
																			memcpy(LiveName, p2+1,strlen(p2+1));
																		}
																	}
																}
															}
														}
													}

												}

												for(iset = 0,n = 0 ,iGot = 0;n<(int)strlen(LiveName);n++)
												{
													if(strstr(LiveName,"-"))
													{
														if(LiveName[n]=='-')
														{
															memset(strtemp,0,sizeof(strtemp));
															memcpy(strtemp,LiveName,n);
															if(n>4)
															{
																iset = 1;
															}
															iGot = 1;
															continue;
														}													 
														if(iGot == 1)
														{
															if(LiveName[n]=='4' && LiveName[n+1]=='K')
															{
																iGot = 0;
																break;
															}
															else if(LiveName[n]>='0'&&LiveName[n]<='9')
															{
																strtemp[strlen(strtemp)] = LiveName[n];
																iset = 1;
															}
															else if(LiveName[n]==' ')
																continue;
															else
															{
																iGot = 0;
																break;
															}
														}
													}
													else
													{
														p1 = strstr(LiveName,"CCTV");
														if(p1)
														{
															if(LiveName[4+n]>0 && LiveName[4+n] <0x80)
															{
																memset(strtemp,0,sizeof(strtemp));
																memcpy(strtemp,LiveName,5+n);
																iset = 1;
															}
															else
															{
																break;
															}
														}
													}
												}
												if(iset == 1)
													strcpy(LiveName,strtemp);
												for(iset = 0,n = 0 ,iGot = 0;n<(int)strlen(LiveName);n++)
												{
													if(LiveName[n] == '-')
													{
														iGot = 1;
													}
													if(iGot == 1)
														LiveName[n] = LiveName[n+1];
												}
												if(iGot == 1)
													LiveName[strlen(LiveName)] = '\0';
												while(1)
												{
													if(LiveName[strlen(LiveName)-1] == 0x0a || LiveName[strlen(LiveName)-1] == 0x0d)
														LiveName[strlen(LiveName)-1] = '\0';
													else
														break;
												}
												continue;
											}
										}
									}
									else
									{
										memset(livesite[SiteCnt],0,sizeof(livesite[SiteCnt]));

										while(1)
										{
											if(cLine[strlen(cLine)-1] == 0x0a || cLine[strlen(cLine)-1] == 0x0d)
												cLine[strlen(cLine)-1] = '\0';
											else
												break;
										}
										for(i = 0,iGot =0,keySet = 0;i<SiteCnt;i++)
										{
											if(!strcmp(livesite[i],cLine))
											{
												iGot = 1;
												break;
											}
										}
										if(iGot == 0)
										{
											memcpy(livesite[SiteCnt++],cLine,strlen(cLine));
											keySet = SiteCnt-1;
										}
										else
										{
											keySet = i;
										}
										if(icut == 1)
										{
											strcpy(LiveName,Config.Groupset[q].GroupName[p]);
										}
										//去重保存
										for(i =  0,iGot = 0;i<LiveData.LiveKindCnt ;i++)
										{
											if(!strcmp(LiveData.LiveKindName[i],LiveName))
											{
												iGot = 1;
												break;
											}
										}


										if(iGot == 0)
										{
											if(LiveData.LiveKindSiteCnt[LiveData.LiveKindCnt]==20)
												;
											else
											{

												strcpy(LiveData.LiveKindName[LiveData.LiveKindCnt],LiveName);
												LiveData.LiveKindSite[LiveData.LiveKindCnt][LiveData.LiveKindSiteCnt[LiveData.LiveKindCnt]]=keySet;
												LiveData.LiveKindSiteCnt[LiveData.LiveKindCnt]++;
												LiveData.LiveKindCnt++;
											}
										}
										else
										{
											if(LiveData.LiveKindSiteCnt[i]==20)
												;
											else
											{
												for( k = 0,iGot = 0;k<LiveData.LiveKindSiteCnt[i];k++)
												{
													if(LiveData.LiveKindSite[i][k] == keySet)
													{
														iGot = 1;
														break;
													}
												}
												if(iGot == 0)
												{
													LiveData.LiveKindSite[i][LiveData.LiveKindSiteCnt[i]]=keySet;
													LiveData.LiveKindSiteCnt[i]++;
												}
											}
										}

										istart = 0;
									}
								}
							}
							fclose(m3uFile);
						}
					}
				}		

				nRet = _findnext(hnd, &data );
			}
			memset(buffSet,0,sizeof(buffSet));
			if(LiveData.LiveKindCnt > 0  && iw ==0 )
			{
				sprintf(buffSet,"{\"group\":\"%s\",\n\"channels\":[\n",Config.GroupName[q]);
				iw = 1;
			}
			for(int n = 0;n<LiveData.LiveKindCnt;n++)
			{
				memset(bufftemp,0,sizeof(bufftemp));
				sprintf(bufftemp,"{\"name\":\"%s\",\"urls\":[" ,LiveData.LiveKindName[n]);
				strcat(buffSet,bufftemp);
				for(j = 0;j<LiveData.LiveKindSiteCnt[n];j++)
				{
					memset(bufftemp,0,sizeof(bufftemp));
					if(j==(LiveData.LiveKindSiteCnt[n]-1))
						sprintf(bufftemp,"\"%s\"" ,livesite[LiveData.LiveKindSite[n][j]]); 
					else
						sprintf(bufftemp,"\"%s\"," ,livesite[LiveData.LiveKindSite[n][j]]);  
					strcat(buffSet, bufftemp);
				}
				if(n == (LiveData.LiveKindCnt-1))
				{
					strcat(buffSet,"]}");
					if(Config.Groupset[q].SetCnt != 1)
					{
						if(p == (Config.Groupset[q].SetCnt-1))
						{
							strcat(buffSet,"\n");
						}
						else
						{
							strcat(buffSet,",\n");
						}
					}
					else
						strcat(buffSet,"\n");
				}
				else
					strcat(buffSet,"]},\n");
			}
			if(LiveData.LiveKindCnt > 0 )
			{
				fputs(buffSet,ResultFile);	
			}

		}
		if(LiveData.LiveKindCnt > 0 )
		{
			if(q == (Config.GroupCnt-1))
				fputs("]}\n\n",ResultFile);
			else
				fputs("]},\n\n",ResultFile);
		}
	}

	// 	memset(buffSet,0,sizeof(buffSet));
	// 	fread_s(buffSet,sizeof(buffSet),sizeof(buffSet),1,liveSelf);
	// 	fputs(buffSet,ResultFile);
	fclose(ResultFile);
	return 0;
}

