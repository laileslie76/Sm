// liveformat.cpp : 定义控制台应用程序的入口点。
//
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <windows.h>
#include <conio.h>
#include <io.h>
#include <dos.h>
#include <direct.h>
#include <time.h>
#include <cmath>
#define Max 500
#define  MaxLen 248000
typedef struct
{
	int LiveKindCnt;		   //台数
	char LiveKindName[Max][50];  //台名
	int LiveKindSite[Max][30];   //每个台地址序号
	int LiveKindSiteCnt[Max];   //台地址个数
}LiveDataStruct;

int main(int argc, char* argv[])
{
	FILE *Tpl = NULL,*m3uFile = NULL,*SelfFilm = NULL,*liveSelf = NULL,*ResultFile = NULL,*livesetfile = NULL;
	Tpl  = fopen("tpl.ini","rb");
	if(Tpl == NULL)
		return 0;

	SelfFilm  = fopen("self.ini","rb");
	if(SelfFilm == NULL)
		return 0;
	liveSelf  = fopen("live.ini","rb");
	if(liveSelf == NULL)
		return 0;	
	livesetfile = fopen("liveset.ini","rb");
	time_t timep; 
	struct tm *p;
	char name[256] = {0};
	char cline [1024] = {0};
	time(&timep);//获取从1970至今过了多少秒，存入time_t类型的timep
	p = localtime(&timep);//用localtime将秒数转化为struct tm结构体
	sprintf(name, "result.json",1900+p->tm_year,1+p->tm_mon,p->tm_mday,p->tm_hour,p->tm_min,p->tm_sec);//把格式化的时间写入字符数组中

	ResultFile  = fopen(name,"wb");
	if(ResultFile == NULL)
		return 0;
	char buff[MaxLen] ={0},bufftemp[MaxLen] = {0},buffChange[MaxLen] = {0},sitetemp[Max] = {0};
	fread_s(buff,MaxLen,MaxLen,1,Tpl);
	char *p1 = NULL,*p2 = NULL;
	int iGotset = 0;
	p1 = strstr(buff," ], \"lives\": [ { ");
	if(p1)
	{
		memset(buffChange,0,sizeof(buffChange));
		memset(bufftemp,0,sizeof(bufftemp));
		fread_s(buffChange,MaxLen,MaxLen,1,SelfFilm);
		memcpy(bufftemp,buff,p1-buff);
		memcpy(bufftemp+(p1-buff),buffChange,strlen(buffChange));
		memcpy(bufftemp+(p1-buff)+strlen(buffChange),p1,strlen(p1));
		memset(buff,0,MaxLen);
		memcpy(buff,bufftemp,MaxLen);
		p1 = strstr(buff,"\"lives\": [ {");
		if(p1)
		{
			iGotset = 0;
			if(livesetfile)
			{
				while(!feof(livesetfile))
				{
					memset(cline,0,sizeof(cline));
					fgets(cline,sizeof(cline),livesetfile);
					if(strstr(cline,"type=txt"))
						iGotset = 1 ;
					break;
				}
			}
			if(iGotset)
			{
				//"lives": [ { "group": "redirect", "channels": [ { "name": "redirect", "urls": [ "proxy://do=live&type=txt&ext=aHR0cHM6Ly9naXRlZS5jb20vamxsaHEvc3cvcmF3L21hc3Rlci95ZC50eHQ=" ] } ] } ], 

				memset(bufftemp,0,sizeof(bufftemp));
				memcpy(bufftemp,buff,p1-buff);

				strcat(bufftemp,"\"lives\": [ { \"group\": \"redirect\", \"channels\": [ { \"name\": \"redirect\", \"urls\": [ \"proxy:\/\/do=live&type=txt&ext=aHR0cHM6Ly9jMW4uY24vcHJveHlsaXZl\" ] } ] } ],");
				p2  = strstr(buff," \"parses\": [ {");
				if(p2)
				{
					strcat(bufftemp,p2);																																
				}
				memset(buff,0,MaxLen);
				memcpy(buff,bufftemp,MaxLen);
			}

			p1 = strstr(buff,"\"wallpaper\": \"") ;
			if(p1)
			{
				memset(bufftemp,0,sizeof(bufftemp));
				memcpy(bufftemp,buff,p1+14-buff);
				strcat(bufftemp,"https:\/\/yonee77.coding.net\/p\/miao\/d\/miao\/git\/raw\/main\/img\/wallpaper_4.jpg\"");
				p2 = strstr(p1,",");
				if(p2)
				{
					strcat(bufftemp,p2);
					memset(buff,0,MaxLen);
					memcpy(buff,bufftemp,MaxLen);
					fputs(buff,ResultFile);
				}
			}
			fclose(ResultFile);

		}
	}

	return 0;
}

